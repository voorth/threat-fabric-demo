### Thread Fabric Assessment

This project contains the code for the ThreatFabric coding assignment.


#### Requirements
- IntelliJ Idea with Android plugins
- A running Android emulator with at least v24

#### How to run
- Run gradle task _installDebug_
- Start the app ThreatFabricDemo
The logcat pane will show the app's output

#### Final thoughts
This was quite the challenge for me, since I had no prior Android development experience.
Also, my holiday season activities caused a significant delay in doing my research and finishing the assessment.
The final setup is woefully sparse, but I think it a reasonable solution, given the requirements.

The service basically performs 3 tasks:

- List the apps that contain an AndroidManifest.xml file. This was necessary  in order to find a suitable target app
- Extract the manifest file form the chosen apk. To my surprise the current Java Zipfile library is quite oldfashioned, its API still providing an Enumeration instead of an Iterator. This made using the JDK8 Stream library not a feasible solution.
- Write the contents of the manifest file to the Log. In order to deal with the unexpected compressed XML file format, I had to resort to using Xavier Gouchets excellent AXML parser.