package nl.voorth.threatfabricdemo;

import android.app.IntentService;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import androidx.annotation.Nullable;
import fr.xgouchet.axml.CompressedXmlParser;
import org.w3c.dom.Document;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ManifestService extends IntentService
{
  private static final String TAG = "ManifestService";

  public ManifestService()
  {
    super(TAG);
  }

  protected void listApks()
  {
    final PackageManager pm = getPackageManager();
    List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
    packages.stream()
      .map(pkg -> pkg.sourceDir)
      .filter(this::hasManifest)
      .sorted()
      .forEach(dir -> Log.i(TAG, dir));
  }

  private boolean hasManifest(String sourceDir)
  {
    try
    {
      ZipFile apk = new ZipFile(sourceDir);
      Enumeration<? extends ZipEntry> entries = apk.entries();
      while (entries.hasMoreElements())
      {
        if (entries.nextElement().getName().equals("AndroidManifest.xml"))
          return true;
      }
      return false;
    }
    catch (IOException e)
    {
      return false;
    }
  }

  @Override
  protected void onHandleIntent(@Nullable Intent intent)
  {
    Log.i(TAG, "Threat Fabric Assessment");
    listApks();

    String sourceDir = "/system/priv-app/Contacts/Contacts.apk";
    Log.i(TAG, String.format("%s/AndroidManifest.xml:", sourceDir));

    try (ZipFile apk = new ZipFile(sourceDir))
    {
      ZipEntry manifestEntry = apk.getEntry("AndroidManifest.xml");

      InputStream inputStream = apk.getInputStream(manifestEntry);
      Document manifest = new CompressedXmlParser().parseDOM(inputStream);

      DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
      DOMImplementationLS impl =
        (DOMImplementationLS)registry.getDOMImplementation("LS");

      LSSerializer serializer = impl.createLSSerializer();
      serializer.getDomConfig().setParameter("format-pretty-print", true);
      String str = serializer.writeToString(manifest);

      for(String line: str.split(System.getProperty("line.separator")))
      {
        Log.i(TAG, line);
      }
    }

    catch (Exception e)
    {
      Log.wtf(TAG, e);
    }

  }
}
